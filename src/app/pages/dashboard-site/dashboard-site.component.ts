import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {NbThemeService} from "@nebular/theme";
import {SitesService} from "../../@core/services/sites.service";


@Component({
  selector: 'ngx-dashboard-site',
  templateUrl: './dashboard-site.component.html',
  styleUrls: ['./dashboard-site.component.scss']
})
export class DashboardSiteComponent implements OnInit, OnChanges {
  @Input() checkchartcell = false;
  @Input() data;
  @Input() filter;
  @Input() chart = 'line';
  @Input()
  public myOptions: any[] = [];
  options: any;
  optionschartcell: any;
  themeSubscription: any;
  @Input() site;
  public showeddata : any;
  constructor(private theme: NbThemeService, public sitesService: SitesService) {

  }


  private random() {
    return Math.round(Math.random() * 100);
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("changed")
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors: any = config.variables;
      const chartjs: any = config.variables.chartjs;
      console.log(config)

      this.options = {
        tooltips: {
          filter: function (tooltipItem) {
            return tooltipItem.datasetIndex !== 0;
          }
        },
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false,
          position: 'right',
          labels: {
            fontColor: chartjs.textColor,
            usePointStyle: true,
            boxWidth: 6,
            filter: function (tooltipItem) {
              return tooltipItem.datasetIndex !== 0;
            }
          },
        },
        hover: {
          mode: 'index',
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Month',
              },
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Value',
              },
              gridLines: {
                display: true,
                color: chartjs.axisLineColor,
              },
              ticks: {
                fontColor: chartjs.textColor,
              },
            },
          ],
        },
      };
    });
    if (this.myOptions.length > 0) {
      this.showeddata = {datasets: [], labels : this.data.labels};
      this.myOptions.forEach(x => {
        if (x.check) {
          // this.cellDashboardData.datasets =  this.cellDashboardData.datasets.filter(y => y.label !== x.name);
          // this.cellDashboardData.datasets.push(this.cellDashboardDatabackup.find(y => y.label === x.name));
          let data1 = this.data.datasets.find(y => y.label === x.name);
          console.log(data1, "data1");
          if (data1 !== null && data1 !== undefined) {
            this.showeddata.datasets.push(data1);
          }
        }
      });
      this.showeddata.datasets.unshift(this.data.datasets.find(y => y.type === 'line'));
    }

    if (this.checkchartcell) {
      this.optionschartcell = this.options;
      this.optionschartcell.tooltips = {
       filter: function(tooltipItem) {
          return tooltipItem.datasetIndex === 0;
        },
        bodyFontSize: 8,
        titleFontSize: 8

      }
    }
  }

}
