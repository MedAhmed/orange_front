import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListSitesRoutingModule } from './list-sites-routing.module';
import { ListSitesComponent } from './list-sites.component';
import { FirstKpiComponent } from './first-kpi/first-kpi.component';
import {NbCardModule} from "@nebular/theme";
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { ProgressComponent } from './progress/progress.component';
import {ListKpiComponent} from "./list-kpi/list-kpi.component";
import {NgxSpinnerModule} from 'ngx-spinner';


@NgModule({
  declarations: [
    ListSitesComponent,
    FirstKpiComponent,
    DragDropComponent,
    ProgressComponent,
    ListKpiComponent,
  ],
  exports: [
    DragDropComponent
  ],
  imports: [
    NbCardModule,
    CommonModule,
    ListSitesRoutingModule,
    NgxSpinnerModule
  ]
})
export class ListSitesModule { }
