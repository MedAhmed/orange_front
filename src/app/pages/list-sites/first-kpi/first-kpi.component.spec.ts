import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstKpiComponent } from './first-kpi.component';

describe('FirstKpiComponent', () => {
  let component: FirstKpiComponent;
  let fixture: ComponentFixture<FirstKpiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirstKpiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstKpiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
