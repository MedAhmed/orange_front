import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MENU_ITEMS} from "../pages-menu";
import {NotFoundComponent} from "../miscellaneous/not-found/not-found.component";
import {ListKpiComponent} from "./list-kpi/list-kpi.component";
import {SitesComponent} from "../sites/sites.component";
import {KpiDetailsComponent} from "./list-kpi/kpi-details/kpi-details.component";
import {ListSitesComponent} from "./list-sites.component";


export const USER_ROUTES: Routes = [
  {
    path: 'details',
    component: SitesComponent,
  }
];

const routes: Routes = [
  {
    path: ':kpiparameter',
    loadChildren: () => import('./list-kpi/list-kpi-routing.module')
       .then(m => m.ListKpiRoutingModule),

  }
  , {
    path: '',
    redirectTo: MENU_ITEMS[0].children[0].link,
    pathMatch: 'full'
  }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListSitesRoutingModule { }
