import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListKpiRoutingModule } from './list-kpi-routing.module';
import { ListKpiComponent } from './list-kpi.component';
import {NbCardModule} from "@nebular/theme";
import {ListSitesModule} from "../list-sites.module";
import { KpiDetailsComponent } from './kpi-details/kpi-details.component';
import {NgxSpinnerModule} from "ngx-spinner";


@NgModule({
  declarations: [
    ListKpiComponent,
    KpiDetailsComponent
  ],
  imports: [
    CommonModule,
    ListKpiRoutingModule,
    NbCardModule,
    ListSitesModule,
    NgxSpinnerModule
  ]
})
export class ListKpiModule { }
