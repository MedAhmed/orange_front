import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {HttpClient, HttpEventType, HttpResponse} from "@angular/common/http";

@Component({
  selector: 'ngx-list-kpi',
  templateUrl: './list-kpi.component.html',
  styleUrls: ['./list-kpi.component.scss']
})
export class ListKpiComponent implements OnInit {
  public details: boolean = false;

  public paramters;
  public percentDone: number;
  uploadSuccess: boolean;

  constructor(private http: HttpClient,
              public router: Router,
              public route: ActivatedRoute,
              private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.details = false;
    this.route.params.subscribe(params => {
      console.log(params['kpiparameter']);
    });
  }

  fileUploadRedirect($event: any) {
    this.uploadAndProgress($event);
    // this.route.params.subscribe(params => {
    //   this.router.navigate(['/pages/sites/' + params['kpiparameter'] + '/details']);
    //   this.details = true;
    // })
  }

  public onRouterOutletActivate(event: any) {
    return false;
  }

  uploadAndProgress(files: File[]) {
    console.log(files)
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f))
    this.route.params.subscribe(params => {
      this.http.post('http://54.38.80.173:8000/files', formData, {
        reportProgress: true,
        observe: 'events',
        params: {
          kpi: params['kpiparameter']
        }
      })
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.spinner.show();
            this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.uploadSuccess = true;
            this.spinner.hide();
            //  this.route.params.subscribe(params => {
                 this.router.navigate(['/pages/sites/' + params['kpiparameter'] + '/details']);
                this.details = true;
            //  });
          }
        }, error => {
          this.spinner.hide();
          alert('cannot be downloaded');
        });
    });
  }
}
