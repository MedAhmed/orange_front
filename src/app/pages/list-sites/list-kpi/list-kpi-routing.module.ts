import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ECommerceComponent} from "../../e-commerce/e-commerce.component";
import {ListKpiComponent} from "./list-kpi.component";
import {KpiDetailsComponent} from "./kpi-details/kpi-details.component";
import {SitesComponent} from "../../sites/sites.component";

const routes: Routes = [
  {
    path: '',
    component: ListKpiComponent,
  },
  {
    path: 'details',
    component: SitesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListKpiRoutingModule { }
