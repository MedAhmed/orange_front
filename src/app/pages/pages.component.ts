import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import {SitesComponent} from "./sites/sites.component";

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet (activate)="handleRouterActivation($event)"></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;
  handleChildChange(event: any) {
    // Handle event here
  }

  handleRouterActivation(component: any) {
    if (component instanceof SitesComponent) {
      console.log(component.data);
      component.data = 3;
      console.log(component.data);
    }
    }
}
