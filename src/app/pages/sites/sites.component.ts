import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {SitesService} from "../../@core/services/sites.service";
import {Site} from "../../@core/models/site.model";
import {FormControl} from "@angular/forms";
import {Observable, of} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {CellService} from "../../@core/services/cell.service";
import {Cell} from "../../@core/models/cell.model";
import {NbCalendarRange, NbWindowService} from "@nebular/theme";
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";

@Component({
  selector: 'ngx-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.scss']
})
export class SitesComponent implements OnInit, OnChanges {
  public myOptions: any[] = [];
  public testcheck = false;
  public data = 2;
  public filterclick = false;
  public sites: Site;
  public cells: Cell;
  public cellDashboardData: any;
  public cellDashboardDatabackup: any = {};
  public chart = 'line';
  public state: boolean = false;
  inputFormControl: FormControl;
  inputFormCellControl: FormControl;
  public cellPage: number;
  public collectionSizeCell: number;
  filteredControlOptions$: Observable<string[]>;
  filteredControlCellOptions$: Observable<string[]>;
  options: string[] = [];
  page: number;
  collectionSize: number;
  singleOption: String;
  displayPaginator: boolean = false;
  displayPaginatorCell: boolean = false;

  firstLoad: boolean = false;
  selectedSite: String;
  public loading: boolean = false;
  cellOptions: string[] = [];
  range: NbCalendarRange<Date>;
  list_detashed_cell: any[] = [];

  constructor(public sitesService: SitesService,
              public cellService: CellService, private windowService: NbWindowService) {

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.data);
  }

  plus() {
    ++this.data;
  }

  ngOnInit(): void {
    this.sitesService.getAllSites().subscribe(res => {
      this.options = res;
      this.inputFormControl = new FormControl();

      this.filteredControlOptions$ = of(this.options);
      this.filteredControlOptions$ = this.inputFormControl.valueChanges
        .pipe(
          startWith(''),
          map(filterString => this.filter(filterString)),
        );
    })
    this.firstLoad = true
    this.filter("");
  }

  private filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    if (value === '' && this.firstLoad) {
      this.sitesService.getAllSitesPaginated(1, '').subscribe(res => {
        this.sites = res;
        this.page = res.page;
        this.displayPaginator = res.total > 10;
        this.collectionSize = res.total;
        let tmp = res.total % res.size;
        this.loading = !this.loading;
        if (tmp > 0) {
          this.collectionSize++;
        }
      })
    }
    return this.options.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  viewCells(sitename: any) {
    if (!this.state) {
      this.state = !this.state;
    }
    this.selectedSite = sitename;
    if (this.state) {
      this.cellService.getAllCellsPerSite(1, sitename).subscribe(res => {
        this.cellOptions = res;
        this.filteredControlCellOptions$ = of(this.cellOptions);
        this.inputFormCellControl = new FormControl();
        this.filteredControlOptions$ = this.inputFormControl.valueChanges
          .pipe(
            startWith(''),
            map(filterString => this.filterCells(filterString)),
          );
      });
    }
  }

  selectOption(option: string) {
    this.sitesService.getAllSitesPaginated(1, option).subscribe(res => {
      this.displayPaginator = res.total > 10;
      this.sites = res;
      this.loading = !this.loading;
    })
  }

  changePage($event: number) {
    this.page = $event
    this.sitesService.getAllSitesPaginated(this.page, '').subscribe(res => {
      this.sites = res;
    })
  }

  private filterCells(value: string): string[] {
    const filterValue = value.toLowerCase();
    if (value === '') {
      this.cellService.getCellsPerSite(1, this.selectedSite, '').subscribe(res => {
        this.cells = res;
        this.cellPage = res.page;
        this.displayPaginatorCell = res.total > 10;
        this.collectionSizeCell = res.total;
      });
    }
    return this.cellOptions.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  selectCellOption(option: string) {
    this.cellService.getCellsPerSite(1, this.selectedSite, option).subscribe(res => {
      this.cells = res;
    });
  }

  changeCellPage($event: number) {
    this.cellPage = $event;
    this.cellService.getCellsPerSite(this.cellPage, this.selectedSite, '').subscribe(res => {
      this.cells = res;
    });
  }

  closeCell() {
    this.state = !this.state;
  }

  detach_cell_chart(res: any) {
    res.datasets.forEach(x => {
      this.list_detashed_cell.push({
        labels: res.labels,
        datasets: [{label: x.label, data: x.data, backgroundColor: x.borderColor}]
      });
    });
    console.log( this.list_detashed_cell, " list_detashed_cell");
  }

  openWindow(contentTemplate, sitename: any) {
    this.selectedSite = sitename;
    this.sitesService.getDashboardCellsPerSite(sitename).subscribe(res => {
      if (res && Object.keys(res).length !== 0 && Object.getPrototypeOf(res) === Object.prototype
        && res.datasets.length > 0 && res.labels.length > 0) {
        let arraylimit = [];
        res.labels = res.labels.map(x => {
          return x.slice(0, 10);
        });
        this.detach_cell_chart(res);
        res.datasets.forEach((x, i) => {
          x.backgroundColor = x.borderColor;
          this.myOptions.push({id: i, name: x.label, color: x.borderColor, check: true});
          // this.selecOptions.push(i);
          // console.log(this.myOptions);
        });
        // this.myOptionsbackup = this.myOptions;

        res.labels.forEach(x => arraylimit.push(80));
        res.datasets.unshift({
          type: 'line',
          label: 'Line Dataset',
          data: arraylimit,
          fill: false,
          borderColor: 'rgb(146,7,24)'
        });
        console.log(res);
        this.cellDashboardData = res;
        // this.cellDashboardDatabackup = res.datasets;
        this.cellDashboardDatabackup = Object.assign({}, {}, this.cellDashboardData)
        this.windowService.open(
          contentTemplate,
          {
            // title: 'site:' + sitename,
            context: {
              text: 'some text to pass into template',
              size: 'lg'
            },
          },
        );
      }
    });
  }

  openDashboardCell(contentTemplate, cell) {
    // this.cellService.getCellDashboard(cell).subscribe(res => {
    //   this.cellDashboardData = res;
    this.windowService.open(
      contentTemplate,
      {
        // title: 'Cell:' + cell,
        context: {
          text: 'some text to pass into template',
          size: 'lg'
        },
      },
    );
    // })
  }


  pickChart(event: any) {
    this.chart = event;
  }

  testcheckfunction(event: any) {
    this.filterclick = !this.filterclick;
    console.log(this.cellDashboardData, "cellDashboardData");
  }
}
