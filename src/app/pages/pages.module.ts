import { NgModule } from '@angular/core';
import {
  NbAutocompleteModule,
  NbButtonModule, NbCalendarRangeModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbMenuModule, NbSpinnerModule
} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { SitesComponent } from './sites/sites.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { DashboardSiteComponent } from './dashboard-site/dashboard-site.component';
import {ChartsModule} from "./charts/charts.module";
import {ChartModule} from "angular2-chartjs";
import {ListSitesModule} from "./list-sites/list-sites.module";
import {DndDirective} from "./list-sites/directives/dnd.directive";
import {MultiselectDropdownModule} from "angular-2-dropdown-multiselect";
import {AngularMultiSelectModule} from "angular2-multiselect-dropdown";
import {NgxBootstrapMultiselectModule} from "ngx-bootstrap-multiselect";

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    NbInputModule,
    NgbModule,
    NbSpinnerModule,
    ChartsModule,
    ChartModule,
    NbCalendarRangeModule,
    ListSitesModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    NgxBootstrapMultiselectModule
  ],
  declarations: [
    PagesComponent,
    SitesComponent,
    DndDirective,
    DashboardSiteComponent,
  ],
})
export class PagesModule {
}
