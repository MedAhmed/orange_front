export class Cell {
  public items: String[];
  public total: number;
  public page: number;
  public size: number;
}
