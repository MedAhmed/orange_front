import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Site} from "../models/site.model";

@Injectable({
  providedIn: 'root'
})
export class SitesService {
  public api: string = environment.api;
  public user: any;
  public httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getAllSitesPaginated(page, site?): Observable<Site> {
    const data = {size: 10, page: page, site: site};
    return this.http.get<Site>(this.api + 'newestsite', {params: data});
  }

  getAllSites(): Observable<string[]> {
    return this.http.get<string[]>(this.api + 'sites');
  }
  //chart site (dashboard single cell)
  getDashboardCellsPerSite(site): Observable<any> {
    return this.http.get<any>(this.api +site+ '/dashboard/cells/');
  }
}
