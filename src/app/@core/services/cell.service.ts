import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cell} from "../models/cell.model";

@Injectable({
  providedIn: 'root'
})
export class CellService {
  public api: string = environment.api;
  public user: any;
  public httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getCellsPerSite(page, site, cell): Observable<Cell> {
    const data = {size: 10, page: page, cell: cell};
    return this.http.get<Cell>(this.api + site + '/cells', {params: data});
  }

  getAllCellsPerSite(page, site): Observable<string[]> {
    const data = {size: 10, page: page};
    return this.http.get<string[]>(this.api + site + '/cells/list');
  }

  getCellDashboard(cell): Observable<any> {
    return this.http.get  <Observable<any>>(this.api + cell + '/dashboard');
  }
}
